# PROJETO API DO BANCO INTER

1. Como compilar e executar a aplicação:
  - Para compilar e executar a aplicação é necessário executar o seguinte comando: mvn spring-boot: run

2. Como executar os testes unitários:
   - Para executar os testes unitários da aplicação é necessário executar o seguinte comando: mvn test - Executa todos os testes feitos no framework Junit.

3. Como acessar o banco de dados em memória (banco de dados H2)
   - Para acessar o banco e dados da aplicação em memória é necessário:
      1. Acessar o seguinte endereço: http://localhost:8080/h2
      2. Inserir a seguinte url do banco de dados: jdbc:h2:mem:digitounico;DB_CLOSE_DELAY=-1
      3. Logar com o usuário: sa e a senha: sa

4. Como acessar a documentação da API com a interface do Framework Swagger
   - Para acessar a documentação da API com a interface do Swagger é necessário: Acessar o seguinte endereço: http://localhost:8080/swagger-ui.html

Os desafios dessa API propostas pelo banco Inter foram todos devidamente compridos.

Desenvolvedor Responsável: Marcel Philippe Abreu Andrade Contatos: (31) 998565849 - marcelpaa@hotmail.com




